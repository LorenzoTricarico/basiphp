<?php 
  // CHECK if user logged
  // https://www.guru99.com/cookies-and-sessions.html
  //$_COOKIE    //output the contents of the cookie array variable 

  $cookieUtente = $_COOKIE["UTENTE"];
  if($cookieUtente != NULL){
    // REDIRECT https://code.tutsplus.com/tutorials/how-to-redirect-with-php--cms-34680
    header("Location: /index.php", TRUE, 302);
    exit();
  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="style.css" >
    <title>Login</title>
</head>

<body>
    <form action="/internetPresence.php" method="post">
        <label for="tipologia">Azione:</label>
        <select name="tipologia" id="tipologia">
          <option value="login">Login</option>
          <option value="crea-utente">Crea Utente</option>
        </select>

        <label for="classe">Classe: </label>
        <input type="credentials" id="classe" name="classe" required autocomplete="off"

           minlength="1" maxlength="8" size="10">

        <label for="cognome">Cognome: </label>
        <input type="credentials" id="cognome" name="cognome" required autocomplete="off"

           minlength="1" maxlength="30" size="50">
           
        <label for="password">Password: </label>
        <input type="password" id="password" name="password" required autocomplete="off"

           minlength="1" maxlength="30" size="50">

        <div class="row">
            <input type="submit" value="Submit">
        </div>
    </form>
</body>
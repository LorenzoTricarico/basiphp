<?php
/*
TODO: DA VEDERE
 x Github e git per versionare
 x Database per sostituire i file di testo ( salvataggio rudimentale) e vantaggi
    pwd: MT33].0-9jXF1@hV
 - Gestione delle sessioni utente, per fare la Login / Logout una sola volta
 - GESTIONE LOGIN CON SESSIONE PROTETTA
 - Gestione del recupero password e gestione delle mail per verificare l'esistenza in fase di login

TODO: Nuove Funzionalità
 - Area Riservata alunno con elenco presenze in formato tabella
 - Area Riservata Professore per vedere tutti gli inserimenti degli alunni della sua classe
 - Versione HTML e CSS Desktop avanzata
 - Versione HTML e CSS Mobile Avanzata
 - Inviare messaggio ad un altro alunno
TODO: Brainstorming
 - .....
*/

/*
connessione al database
https://www.w3schools.com/php/func_mysqli_connect.asp
*/
//"localhost","my_user","my_password","my_db"
$mysqli = new mysqli("localhost","basiPHP","MT33].0-9jXF1@hV","basiPHP");

// Check connection
if ($mysqli ->connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli->connect_error;
    exit();
}


function checkCredentials($classe, $cognome, $password)
{

    global $mysqli;
    
    /*
    Query a database select
    https://www.w3schools.com/php/php_mysql_select.asp
     */
    $sql = "SELECT * FROM `Utenti` WHERE `cognome` = '$cognome' AND `classe` = '$classe' ";
    $utenti = $mysqli->query($sql);
 
    if ($utenti->num_rows > 0) {
        // output data of each row
        while($row = $utenti->fetch_assoc()) {  //scorre un airga per volta
           
            $credentialClass = $row["classe"];
            $credentialSurname = $row["cognome"];
            $credentialPassword = $row["password"];
            $credentialSalt = $row["salt"];
    
            /*
            sha1
            https://www.php.net/manual/en/function.sha1.php
            */
            $hashPassword = sha1("$password$credentialSalt");
    
            /*
            compare tra due stringe ignorando maiuscole
            https://www.php.net/manual/en/function.strnatcasecmp.php
             */
            return strnatcasecmp($credentialPassword, $hashPassword) == 0 ? $row : NULL;
        }
    } 
    return NULL;
}


function existsCredentials($classe, $cognome)
{
    global $mysqli;
    /*
    Query a database select
    https://www.w3schools.com/php/php_mysql_select.asp
     */
    $sql = "SELECT * FROM `Utenti` WHERE `cognome` = '$cognome' AND `classe` = '$classe'";
    $utenti = $mysqli->query($sql);
    return $utenti->num_rows > 0;
}


function creaCredenziali($classe, $cognome, $password)
{
    global $mysqli;
    if (existsCredentials($classe, $cognome)) {
        http_response_code(409);
        return "utente $cognome per la classe $classe già esiste";
    }
    /*
    timestamp
    https://www.php.net/manual/en/function.time.php
    */
    $salt = time();

    /*
    sha1
    https://www.php.net/manual/en/function.sha1.php
    */
    $hashPassword = sha1("$password$salt");

    /*
    https://www.w3schools.com/php/php_mysql_insert.asp
    inserimento nella tabella database */
    $sql = "INSERT INTO `Utenti` (`ID`, `cognome`, `classe`, `password`, `salt`) VALUES (NULL, '$cognome', '$classe', '$hashPassword', '$salt');";

    if ($mysqli->query($sql) === TRUE) {
        $messaggio = "Utente $cognome creato con successo!";
    } else {
        $messaggio = "Errore durante la creazione dell'Utente $cognome<br/> $mysqli->error";
    }

    return $messaggio;
}

function getLoggedUser(){
    $cookieUtente = $_COOKIE["UTENTE"];
    if($cookieUtente == NULL){
      // REDIRECT https://code.tutsplus.com/tutorials/how-to-redirect-with-php--cms-34680
      return NULL;
    }
    $userLogged = json_decode($cookieUtente, TRUE);
    return $userLogged;
}

function salvaPresenza($classe, $cognome, $passowrd, $tipologia, $ora)
{
    global $mysqli;
    $userLogged = getLoggedUser();
    if ($userLogged == NULL) {
        http_response_code(401);
        return "Credenziali non valide";
    }
    $messaggio = "";

    $tabella = null;
    switch($tipologia){
        case "inizio":
            $tabella = "Ingressi";
            break;
        case "fine":
            $tabella = "Uscite";
            break;
        default:
            return "Tipologia non gestita";
    }
    $userId = $userLogged["ID"];
    /*
    https://www.w3schools.com/php/php_mysql_insert.asp
    inserimento nella tabella database */
    $sql = "INSERT INTO `$tabella` (`fk_user_id`) VALUES ( $userId);";


    if ($mysqli->query($sql) === TRUE) {
        $messaggio = "Presenze per $cognome inserita in $tabella!";
    } else {
        $messaggio = "Errore durante la creazione della presenza $cognome<br/> $mysqli->error";
    }
    return $messaggio;
}

function getPresenze($classe, $cognome, $password)
{
    global $mysqli;
    $userLogged = getLoggedUser();
    if ($userLogged == NULL) {
        http_response_code(401);
        return "Credenziali non valide";
    }
    
    $userId = $userLogged["ID"];
    /*
    Query a database select
    https://www.w3schools.com/php/php_mysql_select.asp
     */
    $sql = "SELECT * FROM `Ingressi` WHERE `fk_user_id` = '$userId' ";
    // ID
    // data
    // fk_user_id
    $ingressiRows = $mysqli->query($sql);
    
    $ingressi = "";
    if ($ingressiRows->num_rows > 0) {
        // output data of each row
        while($row = $ingressiRows->fetch_assoc()) {  //scorre una riga per volta
            $data = $row["data"];
            $ingressi .= "<br/> $data";
        }
    } 
    /*
    Query a database select
    https://www.w3schools.com/php/php_mysql_select.asp
     */
    $sql = "SELECT * FROM `Uscite` WHERE `fk_user_id` = '$userId' ";
    // ID
    // data
    // fk_user_id
    $usciteRows = $mysqli->query($sql);
    
    $uscite = "";
    if ($usciteRows->num_rows > 0) {
        // output data of each row
        while($row = $usciteRows->fetch_assoc()) {  //scorre una riga per volta
            $data = $row["data"];
            $uscite .= "<br/> $data";
        }
    } 


    $message = "<h3>INGRESSI:</h3>$ingressi</br><h3>USCITE:</h3>$uscite</br>";
    return $message;
}


/*
come leggere variabili da php da POST
https://www.php.net/manual/en/reserved.variables.post.php
 */
$classe = $_POST["classe"];
$cognome = $_POST["cognome"];
$password = $_POST["password"];
$tipologia = $_POST["tipologia"];

/*
come gestire le date
https://www.php.net/manual/en/datetime.format.php
*/
$nowUtc = new \DateTime('now',  new \DateTimeZone('UTC'));
$nowUtc->setTimezone(new \DateTimeZone('Europe/Rome'));
$ora = $nowUtc->format('Y-m-d H:i:s');


$messaggio = "";
switch ($tipologia) {
    case "fine":
    case "inizio":
        $messaggio = salvaPresenza($classe, $cognome, $password, $tipologia, $ora);
        break;
    case "crea-utente":
        $messaggio = creaCredenziali($classe, $cognome, $password);
        break;
    case "recupero-presenze":
        $messaggio = getPresenze($classe, $cognome, $password);
        break;
    case "login":
        $userLogged = checkCredentials($classe, $cognome, $password);
        if($userLogged == NULL){
            $messaggio = "CREDENZIALI SBAGLAITE!";
            setcookie('UTENTE', null, -1, '/'); 
        }else{
            $messaggio = "Utente loggato con successo";
            setcookie("UTENTE", json_encode($userLogged), time()+ (60*60),'/'); // expires after 1 hours
        }
        
        break;
    default:
        $messaggio = "azione non gestita";
        break;
}

?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="stylesheet" href="style.css">
        <title>Risultato inserimento</title>
    </head>

    <body>
        <h1>
            Messaggio recepito correttamnete
        </h1>
        <h2>
            <?php echo $messaggio; ?>
        </h2>
        <a href="/">inserisci nuova posizione</a>

    </body>
</html>